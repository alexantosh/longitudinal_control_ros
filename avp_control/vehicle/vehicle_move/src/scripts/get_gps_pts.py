#!/usr/bin/env python


import rospy
import roslaunch
import message_filters
import rospkg
import math
import PID_w
import os
from math import sin, cos, sqrt, atan2, radians
from std_msgs.msg import Bool
from sensor_msgs.msg import NavSatFix
import geopy.distance
from std_msgs.msg import String
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3Stamped
from nav_msgs.msg import Odometry
import numpy
import scipy
import time

# Initialize variables
pub1 = None
pub3 = None
pub2 = None
pub4 = None
v1_d = [[]]
v2_d = [[]]
v3_d = [[]]
v4_d = [[]]
location_collect = ""
frame_time = 0.0
collect_request = True
gotdata = False 
lat_point = 0.0
latitu1 = 0.0
longitu1 = 0.0
latitude2 = 0.0
lat1 = 0.0
lat2 = 0.0
lat3 = 0.0
lat4 = 0.0
long_point = 0.0
lng1 = 0.0
lng2 = 0.0
lng3 = 0.0
lng4 = 0.0
ivd = 0.0

P = 0.05
I = 0.00001
D = 0.03
ss_time = 6500
ss_time2 = 5500
ss_time3 = 7000

pid1 = None
pid2 = None
pid3 = None
feedback1 = 0
feedback2 = 0
feedback3 = 0
out_velo1 = 0
out_velo2 = 0
out_velo3 = 0

T = 0                      
LV_vel = 15  
lin_accel = 0.1             
delta_t = 0.1              
u_0 = 0                     
scale = 2.5
dec_v = 3
v2 = 0
v3 = 0.0
v4 = 0.0
a1 = 0.0
a2 = 0.1
a3 = 0.1
a4 = 0.1

vehicle_length = rospy.get_param("/vehicle1/vehicle_length")
desired_distance = vehicle_length * 2   
calculation_distance = 12  
L = 50

v1 = 0.0
v1_o = 0.0
v2_o = 0.0
v3_o = 0.0
v4_o = 0.0
v2_Vel = 0.0


d1 = 0.0
d2 = 0.0
d3 = 0.0
x1 = 0.0
y1 = 0.0
x2 = 0.0
y2 = 0.0
x3 = 0.0
y3 = 0.0
x4 = 0.0
y4 = 0.0


d12 = 0.0
d23 = 0.0
d34 = 0.0

R = 6373000.0
debug_file = None
all_gps = None
lat_last = 0.0
long_last = 0.0
time_current = 0.0
time_last = 0.0
duration_min = 0.0  
min_coord_change = 0.0  

def callbackv1(data):
    global v1_o
    v1_o = data.vector.x
    print("v1_o set to: ", v1_o)


def callbackv2(data):
    global v2_o
    v2_o = data.vector.x
    print("v2_o set to: ", v2_o)


def callbackv3(data):
    global v3_o
    v3_o = data.vector.x
    print("v3_o set to: ", v3_o)


def callbackv4(data):
    global v4_o
    v4_o = data.vector.x
    print("v4_o set to: ", v4_o)


def callback2(data):
    global latitude2
    global lng2
    latitude2 = data.latitude
    lng2 = data.longitude
    global v2_d
    frame_time = data.header.stamp.secs
    v2d = [frame_time, latitude2, lng2]
    v2_d.append(v2d)
    v2_gps = open("v2_gps.csv", "a")
    v2_gps.write(("%f,%f,%f\n") % (frame_time, latitude2, lng2))
    v2_gps.close()
    print("\nSaved v2_data: \n", v2d, "\n\n")


def callback3(data):
    global lat3
    global lng3
    lat3 = data.latitude
    lng3 = data.longitude
    global v3_d
    frame_time = data.header.stamp.secs
    v3d = [frame_time, lat3, lng3]
    v3_d.append(v3d)
    v3_gps = open("v3_gps.csv", "a")
    v3_gps.write(("%f,%f,%f\n")%(frame_time, lat3, lng3))
    v3_gps.close()
    print("\nSaved v3_data: \n", v3d, "\n\n")


def callback4(data):
    global lat4
    global lng4
    lat4 = data.latitude
    lng4 = data.longitude
    global v4_d
    frame_time = data.header.stamp.secs
    v4d = [frame_time, lat4, lng4]
    v4_d.append(v4d)
    v4_gps = open("v4_gps.csv", "a")
    v4_gps.write(("%f,%f,%f\n")%(frame_time, lat4, lng4))
    v4_gps.close()
    print("\nSaved v4_data: \n", v4d, "\n\n")


def subpub_init():
    rospy.Subscriber("/vehicle1/fix", NavSatFix, callback)
    rospy.Subscriber("/vehicle2/fix", NavSatFix, callback2)
    rospy.Subscriber("/vehicle3/fix", NavSatFix, callback3)
    rospy.Subscriber("/vehicle4/fix", NavSatFix, callback4)

   

def callback_s(data1, data2, data3, data4):
    global frame_time, time_last, gotdata
    global lat1, lng1, lat2, lng2, lat3, lng3, lat4, lng4

    frame_time = data1.header.stamp.secs
    time_last = rospy.get_time()
    
    lat1 = data1.latitude
    lng1 = data1.longitude
    lat2 = data2.latitude
    lng2 = data2.longitude
    lat3 = data3.latitude
    lng3 = data3.longitude
    lat4 = data4.latitude
    lng4 = data4.longitude

    gotdata = True

    calculate_all()


def gps_to_odom(gps_data):
    msg.header.stamp = gps.header.stamp         
    msg.header.frame_id = 'base_footprint'      
    msg.pose.pose.position.x = gps_data.latitude    
    msg.pose.pose.position.y = gps_data.longitude   
    msg.pose.pose.position.z = gps_data.altitude    
    msg.pose.pose.orientation.x = 1                 
    msg.pose.pose.orientation.y = 0                
    msg.pose.pose.orientation.z = 0                 
    msg.pose.pose.orientation.w = 0                 
    msg.pose.covariance = {cov_x, 0, 0, 0, 0, 0,    
                            0, cov_y, 0, 0, 0, 0,   
                            0, 0, cov_z, 0, 0, 0,   
                            0, 0, 0, 99999, 0, 0,   
                            0, 0, 0, 0, 99999, 0,   
                            0, 0, 0, 0, 0, 99999}   

    return msg    


def sync_gps_sub():
    v1_data = message_filters.Subscriber("/vehicle1/fix", NavSatFix)
    v2_data = message_filters.Subscriber("/vehicle2/fix", NavSatFix)
    v3_data = message_filters.Subscriber("/vehicle3/fix", NavSatFix)
    v4_data = message_filters.Subscriber("/vehicle4/fix", NavSatFix)

    ts = message_filters.TimeSynchronizer([v1_data, v2_data, v3_data, v4_data], 1)
    ts.registerCallback(callback_s)


def run_sim():
    global gotdata, d12, d23, d34, pub1, pub2, pub3, pub4, LV_vel, lin_accel, T, ss_time, v2_Vel, delta_t, u_0, P, I, D, pid1, pid2, pid3, feedback1, feedback2, feedback3, start_time, last_time, calculation_distance, debug_file
    rospy.init_node('platoon_controller', anonymous=True)

    init_pids()

    start_time = time.time()
    last_time = start_time
    feedback1 = d12
    feedback2 = d23
    feedback3 = d34
    feedback1_old = feedback1
    feedback2_old = feedback2
    feedback3_old = feedback3

    rate = rospy.Rate(100)  # 10Hz

    pub1 = rospy.Publisher('/vehicle1/cmd_vel', Twist, queue_size=10)
    pub2 = rospy.Publisher('/vehicle2/cmd_vel', Twist, queue_size=10)
    pub3 = rospy.Publisher('/vehicle3/cmd_vel', Twist, queue_size=10)
    pub4 = rospy.Publisher('/vehicle4/cmd_vel', Twist, queue_size=10)


    sync_gps_sub()
 
    for i in range(1,600):
        rate.sleep()
    v = u_0 + lin_accel * delta_t
    time_keeper = 0
    acc_dec = True
    acc2 = False
    acc3 = False
    dec = False
    dec2 = False
    dec3 = False
    TV2 = 10  
    TV3 = 5  
    v2_vel = 0.0
    v3_vel = 0.0
    v4_vel = 0.0

    while not rospy.is_shutdown():
	## LV movement criteria varied here
        if acc_dec:
            if v < (LV_vel - 0.001):
                v = u_0 + lin_accel * delta_t
                T = T + 1
            else:
                acc_dec = False

        if v >= (LV_vel - 0.001):
            if time_keeper < ss_time: 
                time_keeper = time_keeper + 1
            else:
                dec2 = True  
                time_keeper = 0  
        if dec2:
            if v > TV2:
                v = decelerate(v, 0.1, 0.1)
            else:  
                dec2 = False

        if v <= TV2 and v > TV3:
            if time_keeper < ss_time2:  
                time_keeper = time_keeper + 1  
            else:
                dec3 = True  
                time_keeper = 0

        if dec3:
            if v > TV3:
                v = decelerate(v, 0.1, 0.1)
            else:  
                dec3 = False

        if v <= TV3 and v > 0:
            if time_keeper < ss_time3:  
                time_keeper = time_keeper + 1  
            else:
                dec = True

        if dec:
            if v > 0:
                v = decelerate(v, 0.1, 0.1)
                print('decelerating ...')

            if v < 0.001:
                v = 0
	## end LV behavior modification criteria


        move1(v)  
	
	# control section 
        if gotdata:
            current_time = time.time()
            feedback1 = d12
            feedback2 = d23
            feedback3 = d34
            dt = current_time - last_time
            pid1.update(feedback1)
            pid1_error = pid1.last_error
            pid1_output = pid1.output
            v2_vel = pid1_output
            feedback1_old = feedback1

            pid2.update(feedback2)
            pid2_error = pid2.last_error
            pid2_output = pid2.output
            v3_vel = pid2_output
            feedback2_old = feedback2

            pid3.update(feedback3)
            pid3_error = pid3.last_error
            pid3_output = pid3.output
            v4_vel = pid3_output
            feedback3_old = feedback3

            kp = pid1.PTerm
            ki = pid1.ITerm
            kd = pid1.DTerm
            last_time = current_time

            gotdata = False
	
	# no reverse motion check
	# can alternatively be implemented in the PID controller.
        if v2_vel < 0:
            v2_vel = 0  
        move2(v2_vel)

        if v3_vel < 0:
            v3_vel = 0 
        move3(v3_vel)

        if v4_vel < 0:
            v4_vel = 0  
        move4(v4_vel)
	# end no reverse motion check
       
        feedback2 = d23
        feedback3 = d34
        # feedback2 = d23
        # feedback3 = d34

        u_0 = v

        rate.sleep()
	# end of control section


def init_pids():
    global pid1, pid2, pid3, d12, d23, d34, feedback1, feedback2, feedback3, calculation_distance
    pid1 = PID_w.PID(P, I, D)
    pid1.setSampleTime(0.01)
    pid1.SetPoint = calculation_distance
    feedback1 = d12
    pid2 = PID_w.PID(P, I, D)
    pid2.setSampleTime(0.01)
    pid2.SetPoint = calculation_distance
    feedback2 = d23
    pid3 = PID_w.PID(P, I, D)
    pid3.setSampleTime(0.01)
    pid3.SetPoint = calculation_distance
    feedback3 = d34


def callback(data):
    # global gotdata
    global lat1, lng1
    lat1 = data.latitude
    lng1 = data.longitude
    lat_point = lat1
    long_point = lng1
    global v3_d
    # frame_time = data.header.stamp.secs
    v1d = [frame_time, lat1, lng1]
    v1_d.append(v1d)
    v1_gps = open("v1_gps.csv", "a")
    v1_gps.write(("%f,%f,%f\n")%(frame_time, latitu1, longitu1))
    v1_gps.close()

def get_odom_dist(x1, y1, x2, y2):
    return math.hypot(x2-x1, y2 - y1)


def calculate_all():
    global d12
    global d23
    global d34
    global frame_time
    global lat1, lng1
    global lat2
    global lat3
    global lat4
    global lng2
    global lng3
    global lng4
    global gotdata
    if (gotdata):
        d23 = get_distance(lat2, lng2, lat3, lng3)
        d34 = get_distance(lat3, lng3, lat4, lng4)
       
        write_all_to_gps_file(time_last, frame_time, lat1, lng1, lat2, lng2, lat3, lng3, lat4, lng4, d12, d23, d34)
    else:
        print("patience, no data yet!\n")


def euclidean_distance(x1, y1, x2, y2):
    euc_dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
    return euc_dist


def accelerate(i_v, acc, d_t):
    f_v = i_v + acc * d_t
    return f_v


def decelerate(i_V, dec, d_t):
    f_v = i_V - dec * d_t
    return f_v


def get_distanceF(lt, lg, lt2, lg2):
    global R
    lat1 = radians(lt)
    lon1 = radians(lg)
    lat2 = radians(lt2)
    lon2 = radians(lg2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c * 1000
    return distance


def get_distance(lt1, lg1, lt2, lg2):

    gps1 = (lt1, lg1)
    gps2 = (lt2, lg2)

    lt1 = radians(lt1)
    lg1 = radians(lg1)
    lt2 = radians(lt2)
    lg2 = radians(lg2)

    dlng = lg2 - lg1
    dlat = lt2 - lt1

    a = sin(dlat / 2)**2 + cos(lt1) * cos(lt2) * sin(dlng / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance1 = R * c  

    distance = geopy.distance.distance(gps1, gps2).meters
       return distance


def write_all_to_gps_file(ros_time, frame_time, lt1, ln1, lt2, ln2, lt3, ln3, lt4, ln4, d12, d23, d34):
    global all_gps

    try:
        all_gps.write(("%f, %f, %f, %f,%f, %f, %f, %f,%f, %f, %f, %f,%f\n") % (ros_time, frame_time, lt1, ln1, lt2, ln2, lt3, ln3, lt4, ln4, d12, d23, d34))
       
    except Exception as e:
        print("Writing to file exception %s\n" % e)


def write_to_debug_file(kp, ki, kd, dist_dt, feedf_speed, pid_out):
    global debug_file

    try:
        debug_file.write(("%f, %f, %f, %f, %f, %f\n")%(kp, ki, kd, dist_dt, feedf_speed, pid_out))

    except Exception as e:
        print("Writing to file exception %s\n" % e)


def move1(velocity=0.5):
    global pub1
    velo_msg = Twist()
    velo_msg.linear.x = velocity
    velo_msg.linear.y = 0
    velo_msg.linear.z = 0
    velo_msg.angular.x = 0
    velo_msg.angular.y = 0
    velo_msg.angular.z = 0
    pub1.publish(velo_msg)


def move2(velocity=0.5):
    global pub2
    velo_msg = Twist()
    velo_msg.linear.x = velocity
    velo_msg.linear.y = 0
    velo_msg.linear.z = 0
    velo_msg.angular.x = 0
    velo_msg.angular.y = 0
    velo_msg.angular.z = 0
    pub2.publish(velo_msg)


def move3(velocity=0.5):
    global pub3
    velo_msg = Twist()
    velo_msg.linear.x = velocity
    velo_msg.linear.y = 0
    velo_msg.linear.z = 0
    velo_msg.angular.x = 0
    velo_msg.angular.y = 0
    velo_msg.angular.z = 0
    pub3.publish(velo_msg)


def move4(velocity=0.5):
    global pub4
    velo_msg = Twist()
    velo_msg.linear.x = velocity
    velo_msg.linear.y = 0
    velo_msg.linear.z = 0
    velo_msg.angular.x = 0
    velo_msg.angular.y = 0
    velo_msg.angular.z = 0
    pub4.publish(velo_msg)


if __name__ == '__main__':
    try:
        run_sim()  

    except rospy.ROSInterruptException:
        gps_file.close()
        move1(0)
        all_gps.close()
        fused_data_file.close()
        debug_file.close()
        rospy.loginfo("closed files ...")
        pass
